import os
from typing import List

import nbformat
from pathlib import Path

from dataclasses import dataclass
from jinja2 import Environment, select_autoescape, FileSystemLoader
from nbconvert.preprocessors import ExecutePreprocessor


@dataclass
class Change:
    date: str
    items: List[str]


changes = [
    Change(date='16 janvier 2022', items=[
        'Première version'
    ]),
    Change(date='22 janvier 2022', items=[
        'ajout des vidéos', "amélioration de la version pour mobile"
    ]),
    Change(date='30 janvier 2022', items=[
        "mise à jour des données"
    ])
]


def main():
    with open("Analyse par âges.ipynb") as f:
        age = nbformat.read(f, as_version=4)

        with open("Analyse par status vaccinal.ipynb") as f:
            vaccin = nbformat.read(f, as_version=4)

    ep = ExecutePreprocessor(timeout=600, kernel_name='python3')
    ep.preprocess(age, {'metadata': {'path': './'}})
    ep.preprocess(vaccin, {'metadata': {'path': './'}})

    env = Environment(
        loader=FileSystemLoader('templates'),
        autoescape=select_autoescape(['html', 'xml'])
    )

    analysis = env.get_template('analysis.html')
    sections = {}
    for folder in os.listdir("templates/export"):
        if not folder.startswith("."):
            sections[folder] = [f"{folder}/{file}"
                                for file in os.listdir(f"templates/export/{folder}")
                                if file.endswith(".html")]

            path = Path(f"public/{folder}.html")
            path.parent.mkdir(exist_ok=True)
            path.write_text(analysis.render(sections=sections[folder], title=folder, changes=changes))

    index = Path("public/index.html")
    index.write_text(env.get_template('index.html').render(sections=sections, changes=changes))


if __name__ == '__main__':
    main()
